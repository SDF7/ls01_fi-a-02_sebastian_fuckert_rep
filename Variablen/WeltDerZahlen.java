/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  *
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author Sebastian Fuckert
  */

public class WeltDerZahlen {

  public static void main(String[] args) {

    /*  *********************************************************

         Zuerst werden die Variablen mit den Werten festgelegt!

    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem
    byte anzahlPlaneten = 8;

    // Anzahl der Sterne in unserer Milchstraße
    long anzahlSterne = 250000000000L;

    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3450889;

    // Wie alt bist du?  Wie viele Tage sind das?
    short alterTage = 11341;

    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 200000;

    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    int flaecheGroessteLand = 17098242;

    // Wie groß ist das kleinste Land der Erde?
    float flaecheKleinsteLand = 0.44f;




    /*  *********************************************************

         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen

    *********************************************************** */

    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);

    System.out.println("Anzahl der Sterne: " + anzahlSterne);

    System.out.println("Anzahl der Einwohner Berlins: " + bewohnerBerlin);

    System.out.println("Mein Alter: " + alterTage + " Tage");

    System.out.println("Gewicht des schwersten Tieres der Welt: " + gewichtKilogramm + " kg");

    System.out.println("Fläche des größten Landes der Welt: " + flaecheGroessteLand + " km²");

    System.out.println("Fläche des kleinsten Landes der Welt: " + flaecheKleinsteLand + " km²");

    System.out.println(" *******  Ende des Programms  ******* ");

  }
}
