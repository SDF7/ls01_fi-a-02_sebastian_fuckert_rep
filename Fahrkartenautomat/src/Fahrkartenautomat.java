﻿import java.util.Scanner;
import java.util.stream.IntStream;

class Fahrkartenautomat {
    public static void main(String[] args) {
        Fahrkartenautomat automat = new Fahrkartenautomat();
        Scanner tastatur = new Scanner(System.in);
        boolean running = true;
        double zuZahlenderBetrag;
        double rückgabebetrag;


        while (running) {
            zuZahlenderBetrag = automat.fahrkartenbestellungErfassen();

            rückgabebetrag = automat.fahrkartenBezahlen(zuZahlenderBetrag);

            automat.fahrkartenAusgeben();

            automat.rueckgeldAusgeben(rückgabebetrag);

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                    + "Wir wünschen Ihnen eine gute Fahrt.\n");

            String decision = "";
            do {
                System.out.print("Móchten Sie einen weiteren Kaufvorgang starten?(j/n) ");
                decision = tastatur.next();

                if (decision.equals("n")) {
                    running = false;
                } else if (decision.equals("j")) {
                    System.out.println();
                } else {
                    System.out.println("Bitte geben Sie j oder n ein.");
                }
            } while (!decision.equals("n") && !decision.equals("j"));
        }
    }

    private double fahrkartenbestellungErfassen() {
        Scanner tastatur = new Scanner(System.in);
        // Arrays haben den Vorteil das man alle Elemente mit
        // einer Schleife effektiv abfragen kann.
        // Zusätzlich reicht es Elemente nur im Array anzupassen,
        // da man auf Arrays per Index zugreift.

        // Die neue Implemetierung macht es uns leicht neue Tickets zum
        // Automaten hinzuzufügen oder zu entfernen. Zusätzlich passt sich
        // unseren Output und Logik dynamisch an.
        // Ein Nachteil ist das beide Arrays voneinenader unabhängig sind.
        // Man muss aufpassen das beide Arrays gleich viele Elemente besitzen
        // oder es kann zu OutOfBounds Exceptions kommen.
        // Es wäre besser entweder ein 2D Array zu benutzen oder Tickets als
        // eine Klasse zu implementieren.
        String[] fahrkartenBezeichnungen = {
            "Einzelfahrschein Berlin AB",
            "Einzelfahrschein Berlin BC",
            "Einzelfahrschein Berlin ABC",
            "Kurzstrecke",
            "Tageskarte Berlin AB",
            "Tageskarte Berlin BC",
            "Tageskarte Berlin ABC",
            "Kleingruppen-Tageskarte Berlin AB",
            "Kleingruppen-Tageskarte Berlin BC",
            "Kleingruppen-Tageskarte Berlin ABC" };
            double[] fahrkartenPreise = {
                2.90,
                3.30,
                3.60,
                1.90,
                8.60,
                9.00,
                9.60,
                23.50,
                24.30,
                24.90 };

        int auswahl = 0;
        do {
            System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin aus:");
            for (int i = 0; i < fahrkartenPreise.length; i++) {
                System.out.printf("\t%s [%.2f] (%d)\n", fahrkartenBezeichnungen[i], fahrkartenPreise[i], i + 1);
            }
            System.out.print("Ihre Wahl: ");
            auswahl = tastatur.nextInt();

            if(auswahl < 1 || auswahl > fahrkartenPreise.length) {
                System.out.println(">>falsche Eingabe<<\n");
            }
        } while (auswahl < 1 || auswahl > fahrkartenPreise.length);


        System.out.print("Anzahl der Fahrkarten: ");
        int anzahlDerFahrkarten = tastatur.nextInt();

        while (anzahlDerFahrkarten < 1 || anzahlDerFahrkarten > 10) {
            System.out.println("Ungültige Anzahl an Fahrkarten. Bitte geben Sie einen Wert zwischen 1(Eins) und 10(Zehn) ein.");
            System.out.print("Anzahl der Fahrkarten: ");
            anzahlDerFahrkarten = tastatur.nextInt();
        }

        return fahrkartenPreise[auswahl - 1] * anzahlDerFahrkarten;
    }

    private boolean münzeIstGültig(double münze) {
        switch (Double.toString(münze)) {
            case "2.0":
            case "1.0":
            case "0.5":
            case "0.2":
            case "0.1":
            case "0.05":
                return true;
            default:
                System.out.println("Kein gültiger Wert für eine Münze!\nErlaubte Werte: 2 Euro(2.0), 1 Euro(1.0), 50Ct(0.5), 20Ct(0.2), 10Ct(0.1), 5Ct(0.05)\n");
                return false;
        }
    }

    private double fahrkartenBezahlen(double zuZahlenderBetrag) {
        // Geldeinwurf
        // -----------
        Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMünze;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            do {
                System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
                System.out.print("Eingabe (mind. 5Ct(0.5), höchstens 2 Euro(2.0)): ");
                eingeworfeneMünze = tastatur.nextDouble();
            } while (!münzeIstGültig(eingeworfeneMünze));

            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }

        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    private void fahrkartenAusgeben() {
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n");
    }

    private boolean isEuro(int münze) {
        return münze == 1 || münze == 2;
    }

    private boolean isCent(int münze) {
        return münze == 5 || münze == 10 || münze == 20 || münze == 50;
    }

    private void druckeMünzen(int[] münzen, int count) {
        IntStream.range(0, count).forEach(i -> System.out.print("   * * *    "));
        System.out.println();
        IntStream.range(0, count).forEach(i -> System.out.print(" *       *  "));
        System.out.println();
        for (int i = 0; i < count; i++) {
            if (isEuro(münzen[i])) {
                System.out.printf("*%5d    * ", münzen[i]);
            } else if (isCent(münzen[i])) {
                System.out.printf("*    %-5d* ", münzen[i]);
            }
        }
        System.out.println();
        for (int i = 0; i < count; i++) {
            if (isEuro(münzen[i])) {
                System.out.printf("*   EURO  * ");
            } else if (isCent(münzen[i])) {
                System.out.printf("*   CENT  * ");
            }
        }
        System.out.println();
        IntStream.range(0, count).forEach(i -> System.out.print(" *       *  "));
        System.out.println();
        IntStream.range(0, count).forEach(i -> System.out.print("   * * *    "));
        System.out.println();

    }

    private void rueckgeldAusgeben(double rückgabebetrag) {
        // Rückgeldberechnung und -Ausgabe
        // -------------------------------

        // Konvertiereung zu Ganzzahl um Rechenfehler wegen Ungenauigkeit zu verhindern
        long ganzzahlRückgabebetrag = Math.round(rückgabebetrag * 100);

        if (ganzzahlRückgabebetrag > 0) {
            int[] münzen = new int[3];
            int count = 0;
            while (ganzzahlRückgabebetrag >= 200) // 2 EURO-Münzen
            {
                ganzzahlRückgabebetrag -= 200;
                münzen[count] = 2;
                count++;
                if (count == 3 || ganzzahlRückgabebetrag == 0) {
                    druckeMünzen(münzen, count);
                    count = 0;
                }
            }
            while (ganzzahlRückgabebetrag >= 100) // 1 EURO-Münzen
            {
                ganzzahlRückgabebetrag -= 100;
                münzen[count] = 1;
                count++;
                if (count == 3 || ganzzahlRückgabebetrag == 0) {
                    druckeMünzen(münzen, count);
                    count = 0;
                }
            }
            while (ganzzahlRückgabebetrag >= 50) // 50 CENT-Münzen
            {
                ganzzahlRückgabebetrag -= 50;
                münzen[count] = 50;
                count++;
                if (count == 3 || ganzzahlRückgabebetrag == 0) {
                    druckeMünzen(münzen, count);
                    count = 0;
                }
            }
            while (ganzzahlRückgabebetrag >= 20) // 20 CENT-Münzen
            {
                ganzzahlRückgabebetrag -= 20;
                münzen[count] = 20;
                count++;
                if (count == 3 || ganzzahlRückgabebetrag == 0) {
                    druckeMünzen(münzen, count);
                    count = 0;
                }
            }
            while (ganzzahlRückgabebetrag >= 10) // 10 CENT-Münzen
            {
                ganzzahlRückgabebetrag -= 10;
                münzen[count] = 10;
                count++;
                if (count == 3 || ganzzahlRückgabebetrag == 0) {
                    druckeMünzen(münzen, count);
                    count = 0;
                }
            }
            while (ganzzahlRückgabebetrag >= 5)// 5 CENT-Münzen
            {
                ganzzahlRückgabebetrag -= 5;
                münzen[count] = 5;
                count++;
                if (count == 3 || ganzzahlRückgabebetrag == 0) {
                    druckeMünzen(münzen, count);
                    count = 0;
                }
            }
            System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");
        }
    }
}