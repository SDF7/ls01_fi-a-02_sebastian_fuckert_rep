import java.util.Scanner;

public class Schleifen {

    public static void main(String[] args) {
        stair(5, 3);
        million(100000, 20.0);
    }

    public static void stair(int height, int width) {
        int length = height  * width;

        for (int i = height - 1; i >= 0; i--) {
            int numberStars = length - width * i;
            int numberSpaces = length - numberStars;

            for (int j = 0; j < numberSpaces; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < numberStars; j++) {
                System.out.print("*");
            }
            System.out.print("\n");
        }
    }

    public static void million(long deposit, double interestRate) {
        Scanner keyboard = new Scanner(System.in);
        boolean running = true;
        int years = 1;
        long initialDeposit = deposit;

        while(running) {
            while (deposit < 1000000) {
                deposit += deposit * (interestRate / 100);
                years++;
            }

            System.out.printf("With a initial deposit of %d and a interest rate of %.2f%%, becoming a millionaire will take %d years.\n", initialDeposit, interestRate, years);

            String decision = "";
            do {
                System.out.print("Do you want to do another calculation?(y/n) ");
                decision = keyboard.next();
                if (decision.equals("n")) {
                    running = false;
                } else if (decision.equals("y")) {
                    System.out.print("Input new deposit: ");
                    initialDeposit = keyboard.nextLong();
                    deposit = initialDeposit;
                    System.out.print("Input new interest rate: ");
                    interestRate = keyboard.nextDouble();
                } else {
                    System.out.println("Please input y or n.");
                }
            } while (!decision.equals("n") && !decision.equals("y"));
        }
    }
}