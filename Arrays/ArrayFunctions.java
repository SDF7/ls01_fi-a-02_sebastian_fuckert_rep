public class ArrayFunctions {

    public static String convertArrayToString(int[] zahlen) {
        String arrayString = "";

        for (int i = 0; i < zahlen.length; i++) {
            if(i != zahlen.length - 1) {
                arrayString += Integer.toString(zahlen[i]) + ",";
            } else {
                arrayString += Integer.toString(zahlen[i]);
            }
        }
        return arrayString;
    }

     public static int[] reverseArrayInPlace(int[] array) {

        for (int i = 0; i < array.length / 2; i++) {
            int swap = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = swap;
        }
        return array;
     }

     public static int[] reverseArray(int[] array) {
        int[] reverseArray = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            reverseArray[array.length - 1 - i] = array[i];
        }

        return reverseArray;
     }

     public static double[][] createTemperatureTable(int numberOfRows) {
        double[][] temperatureTable = new double[numberOfRows][2];

        for (int i = 0; i < temperatureTable.length; i++) {
            temperatureTable[i][0] = 10.0 * i;
            temperatureTable[i][1] = (5.0/9.0) * ((10.0 * i) - 32);
        }

        return temperatureTable;
     }

     public static int[][] createMatrix(int n, int m, int[] values) {
         int[][] matrix = new int[n][m];

         // If input values length is not equal n*m matrix length return default matrix
        if (values.length != n * m) {
            return matrix;
        }

        // Build row wise
        for (int i = 0; i < values.length; i++) {
            int ni = i / n;
            int mi = i % n;
            matrix[mi][ni] = values[i];
        }

        return  matrix;
     }

     public static boolean isSymmetric(int[][] matrix) {
        // Only square matrices can be symmetric
        if (matrix.length != matrix[0].length) {
            return false;
        }

        for (int i = 0; i < matrix.length; i++) {
            // Start at i to skip reverse comparisons (i,j == j,i) == (j,i == i,j)
            for (int j = i; j < matrix[0].length; j++) {
                if (matrix[i][j] != matrix[j][i]) {
                    return false;
                }
            }
        }

        return true;
     }

    public static void main(String[] args) {
        int[] testArray = new int[]{3, 7, 12, 18, 37, 42};

        // Aufgabe 1
        System.out.println(convertArrayToString(testArray));
        System.out.println();

        // Aufgabe 2
        int[] evenArray = new int[]{3, 7, 12, 18};
        int[] oddArray = new int[]{3, 7, 12, 18, 37};
        System.out.println(convertArrayToString(reverseArrayInPlace(evenArray)));
        System.out.println(convertArrayToString(reverseArrayInPlace(oddArray)));
        System.out.println();

        // Aufgabe 3
        int[] evenArray2 = new int[]{3, 7, 12, 18};
        int[] oddArray2 = new int[]{3, 7, 12, 18, 37};
        System.out.println(convertArrayToString(reverseArray(evenArray2)));
        System.out.println(convertArrayToString(reverseArray(oddArray2)));
        System.out.println();

        // Aufgabe 4
        int rows = 5;
        double[][] temperatureTable = createTemperatureTable(rows);

        for (int i = 0; i < rows; i++) {
            System.out.printf("%4.1f °F == %6.2f °C\n", temperatureTable[i][0], temperatureTable[i][1]);
        }
        System.out.println();

        // Aufgabe 5
        int n = 5;
        int m = 4;
        int[] values = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ,13, 14, 15, 16, 17, 18, 19, 20};
        int[][] matrix = createMatrix(n, m, values);

        for (int mi = 0; mi < m; mi++) {
            for (int ni = 0; ni < n; ni++) {
                System.out.printf("%d ", matrix[ni][mi]);
            }
            System.out.println();
        }
        System.out.println();

        // Aufgabe 6
        int[][] sym = { {1, 2, 3},
                        {2, 4, 5},
                        {3, 5, 6}};
        System.out.println("Is matrix symmetric? : " + isSymmetric(matrix));
        System.out.println("Is matrix symmetric? : " + isSymmetric(sym));
    }
}
