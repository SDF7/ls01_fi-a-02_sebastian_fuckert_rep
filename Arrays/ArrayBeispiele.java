public class ArrayBeispiele {
    public static void main(String[] args) {

        int[] zahlenliste;
        zahlenliste = new int[6];
        zahlenliste[0] = 11;
        zahlenliste[3] = 42;
        zahlenliste[2] = 10;

        System.out.println(zahlenliste[2]);

        for (int i = 0; i < zahlenliste.length; i++) {
            zahlenliste[i] = i + 1;
        }

        for (int i = 0; i < zahlenliste.length; i++) {
            System.out.print(zahlenliste[i] + ",");
        }
        System.out.println();

        for (int i = 0; i < zahlenliste.length; i++) {
            zahlenliste[i] = 2 * i + 1;
        }

        for (int i = 0; i < zahlenliste.length; i++) {
            System.out.print(zahlenliste[i] + ",");
        }
        System.out.println();

        for (int i = 5; i >= 0; i--) {
            zahlenliste[zahlenliste.length - 1 - i] = 2 * i + 1;
        }

        for (int i = 0; i < zahlenliste.length; i++) {
            System.out.print(zahlenliste[i] + ",");
        }
        System.out.println();

    }
}