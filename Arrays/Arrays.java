import java.util.Scanner;

public class Arrays {

    public static void zahlen() {
        int[] zahlen = new int[10];

        for (int i = 0; i < zahlen.length; i++) {
            zahlen[i] = i;
        }

        for (int i = 0; i < zahlen.length; i++) {
            System.out.printf("%d ", zahlen[i]);
        }
        System.out.println();

    }

    public static void ungeradezahlen() {
        int[] ungeradeZahlen = new int[10];
        int index = 0;

        for (int zahl = 1; zahl <= 19; zahl++) {
            if (zahl % 2 != 0) {
                ungeradeZahlen[index] = zahl;
                index++;
            }
        }

        for (int i = 0; i < ungeradeZahlen.length; i++) {
            System.out.printf("%d ", ungeradeZahlen[i]);
        }
        System.out.println();

    }

    public static void palindrom() {
        char[] charArray = new char[5];
        Scanner eingabe = new Scanner(System.in);

        for (int i = 0; i < 5; i++) {
            charArray[i] = eingabe.nextLine().charAt(0);
        }

        for (int i = charArray.length - 1; i >= 0; i--) {
            System.out.printf("%s", charArray[i]);
        }
        System.out.println();

    }

    public static void lotto() {
        int[] lottoZahlen = {3, 7, 12, 18, 37, 42};
        boolean foundTwelve = false;
        boolean foundThirteen = false;

        System.out.print("[ ");
        for (int i = 0; i < lottoZahlen.length; i++) {
            System.out.printf("%s ", lottoZahlen[i]);
        }
        System.out.println("]");

        for (int z : lottoZahlen) {
            if (z == 12)
                foundTwelve = true;
            if (z == 13)
                foundThirteen = true;
        }

        if (foundTwelve) {
            System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
        } else {
            System.out.println("Die Zahl 12 ist nichtin der Ziehung enthalten.");
        }
        if (foundThirteen) {
            System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
        } else {
            System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
        }

    }

    public static void main(String[] args) {
        zahlen();
        ungeradezahlen();
        palindrom();
        lotto();
    }
}
