import java.io.Serializable;

public class BenutzerListe implements Serializable {

    private static final long serialVersionUID = -5197841881047088760L;

    private Benutzer first;
    private Benutzer last;

    public BenutzerListe() {
        first = last = null;
    }

    public void insert(Benutzer b) {
        // Sicherheitshalber setzen wir
        // den Nachfolger auf
        // null:
        b.setNext(null);
        if (first == null) {
            first = last = b;
        } else {
            last.setNext(b);
            last = b;
        }
    }

    public String select() {
        String s = "";
        Benutzer b = first;
        while (b != null) {
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }

    public String select(String name) {
        Benutzer b = first;
        while (b != null) {
            if (b.hasName(name)) {
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }

    public Benutzer getBenutzer(String name) {
        Benutzer b = first;
        while (b != null) {
            if (b.hasName(name)) {
                return b;
            }
            b = b.getNext();
        }
        return null;
    }

    public boolean delete(String name){
        if (first == null) {
            return false;
        }

        if (first.getBenutzername().equals(name)) {
            if (first == last) {
                // Da first == last haben wir nur ein Element in der Liste.
                // Also setzen wir last mit first.getNext() auf null.
                last = first.getNext();
            }
            first = first.getNext();

            return true;
        }

        Benutzer b = first;

        while (b != null) {
            if (b.getNext().getBenutzername().equals(name)) {
                if (b.getNext() == last) {
                    last = b;
                }
                b.setNext(b.getNext().getNext());
                return true;
            }
            b = b.getNext();
        }

        return false;
    }

    public String toStringObject() {
        String stringObject = "";
        Benutzer b = first;

        while (b != null) {
            stringObject += b.toStringObject() + "\n";
            b = b.getNext();
        }

        return stringObject;
    }
}
