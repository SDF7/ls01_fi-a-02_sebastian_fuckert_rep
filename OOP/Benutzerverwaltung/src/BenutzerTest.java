import java.time.LocalDateTime;
import java.util.Arrays;

public class BenutzerTest {
    public static void main(String[] args) {
        Benutzer simpel = new Benutzer("Simpel", Crypto.encryptViginere("passwort".toCharArray()));
        Benutzer komplex = new Benutzer("Peter", "Lustig", "Komplex", Crypto.encryptViginere("passwort".toCharArray()), "peter.lustig@löwenzahn.de");

        System.out.println("Test Simpeler Benutzer Konstruktor: " + testSimpelKonstruktor(simpel));
        System.out.println("Test komplexer Benutzer Konstruktor: " + testKomplexKonstruktor(komplex));
        System.out.println("Test Benutzer Getter/Setter: " + testGetterSetter(simpel));

        System.out.println(benutzerDrucken(simpel));
    }

    public static String benutzerDrucken(Benutzer benutzer) {
        return "Benutzer [benutzername=" + benutzer.getBenutzername() + ", email=" + benutzer.getEmail() + ", istAdmin=" + benutzer.getIstAdmin()
                + ", istAngemeldet=" + benutzer.getIstAngemeldet() + ", letzterLogin=" + benutzer.getLetzterLogin() + ", nachname=" + benutzer.getNachname()
                + ", passwort=" + new String(benutzer.getPasswort()) + ", vorname=" + benutzer.getVorname() + ", next=" + benutzer.getNext() + "]";
    }

    public static boolean testSimpelKonstruktor(Benutzer simpel) {
        if (simpel.getBenutzername() != "Simpel") { return false; }
        if (simpel.getVorname() != "") { return false; }
        if (simpel.getNachname() != "") { return false; }
        if (simpel.getEmail() != "") { return false; }
        if (!Arrays.equals(simpel.getPasswort(), Crypto.encryptViginere("passwort".toCharArray()))) { return false; }
        if (simpel.getIstAdmin() != false) { return false; }
        if (simpel.getIstAngemeldet() != false) { return false; }
        if (simpel.getLetzterLogin() != "") { return false; }
        if (simpel.getNext() != null) { return false; }

        return true;
    }

    public static boolean testKomplexKonstruktor(Benutzer komplex) {
        if (komplex.getBenutzername() != "Komplex") { return false; }
        if (komplex.getVorname() != "Peter") { return false; }
        if (komplex.getNachname() != "Lustig") { return false; }
        if (komplex.getEmail() != "peter.lustig@löwenzahn.de") { return false; }
        if (!Arrays.equals(komplex.getPasswort(), Crypto.encryptViginere("passwort".toCharArray()))) { return false; }
        if (komplex.getIstAdmin() != false) { return false; }
        if (komplex.getIstAngemeldet() != false) { return false; }
        if (komplex.getLetzterLogin() != "") { return false; }
        if (komplex.getNext() != null) { return false; }

        return true;
    }

    public static boolean testGetterSetter(Benutzer benutzer) {
        LocalDateTime aktuelleZeit = LocalDateTime.now();
        String testZeitpunkt = aktuelleZeit.toString();

        benutzer.setBenutzername("NN");
        benutzer.setVorname("Neuer");
        benutzer.setNachname("Name");
        benutzer.setEmail("nn@test.de");
        benutzer.setPasswort(Crypto.encryptViginere("newPasswort".toCharArray()));
        benutzer.setIstAdmin(true);
        benutzer.setIstAngemeldet(true);
        benutzer.setLetzterLogin(testZeitpunkt);
        benutzer.setNext(benutzer);

        if (benutzer.getBenutzername() != "NN") { return false; }
        if (benutzer.getVorname() != "Neuer") { return false; }
        if (benutzer.getNachname() != "Name") { return false; }
        if (benutzer.getEmail() != "nn@test.de") { return false; }
        if (!Arrays.equals(benutzer.getPasswort(), Crypto.encryptViginere("newPasswort".toCharArray()))) { return false; }
        if (benutzer.getIstAdmin() != true) { return false; }
        if (benutzer.getIstAngemeldet() != true) { return false; }
        if (benutzer.getLetzterLogin() != testZeitpunkt) { return false; }
        if (benutzer.getNext() != benutzer) { return false; }

        return true;
    }
}
