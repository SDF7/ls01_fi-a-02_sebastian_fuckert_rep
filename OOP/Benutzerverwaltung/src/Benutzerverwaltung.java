import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Benutzerverwaltung {

    public static void main(String[] args) {

        try {
            BenutzerverwaltungV30.start();
        } catch (Exception e) {
            System.out.println("Schreiben/Lesen von BenutzerListe.bin/.txt ist schief gelaufen.");
            Path benutzerListeBinärPfad = Paths.get("BenutzerListe.bin");
            Path benutzerListeKlartextPfad = Paths.get("BenutzerListe.txt");
            try {
                Files.deleteIfExists(benutzerListeBinärPfad);
                Files.deleteIfExists(benutzerListeKlartextPfad);
            } catch (Exception e1) {
                System.out.println("BenutzerListe.bin/.txt konnte nach einem Fehler nicht gelöscht werden, bitte manuell löschen.");
            }
        }

    }
}

class BenutzerverwaltungV30{
    static Console console = System.console();

    public static void start() throws Exception {
        Benutzer benutzer = null;
        // BenutzerListe benutzerListe = ladeBenutzerlisteBinär();
        BenutzerListe benutzerListe = ladeBenutzerListeKlartext();

        // console.printf(new String(Crypto.decryptCaesar(new char[]{36, 61, 72, 72, 75, 13, 14, 15})) + "\n"); // Admin passwort ist: Hallo123

        console.printf("Menü\n");
        console.printf("\t1 - Anmelden\n");
        console.printf("\t2 - Registrieren\n");

        String auswahl;
        auswahl = console.readLine("\nAuswahl: ");

        while (!auswahl.equals("1") && !auswahl.equals("2")) {
            console.printf("\tUngültige Eingabe. Bitte wáhle 1 oder 2.\n");
            auswahl = console.readLine("\nAuswahl: ");
            console.format("\tAuswahl: %s\n", auswahl);
        }

        if (auswahl.equals("1")) {
            console.printf("\nAnmelden\n");
            console.printf("\tBitte geben Sie Ihre Zugangsdaten ein.\n\n");
            String benutzername;
            char[] passwort;
            benutzername = console.readLine("\tBenutzername: ");
            passwort = Crypto.encryptViginere(console.readPassword("\tPasswort: "));
            int versuche = 0;
            versuche++;

            while (!authenticate(benutzerListe, benutzername, passwort)) {
                if (versuche == 3) {
                    console.printf("\tDritter Fehlversuch. Auf Wiedersehen.\n");
                    return;
                }
                console.printf("\tFalscher Name oder falsches Passwort.\n\n");
                console.printf("\tBitte geben Sie Ihre Zugangsdaten ein.\n\n");
                benutzername = console.readLine("\tBenutzername: ");
                passwort = Crypto.encryptViginere(console.readPassword("\tPasswort: "));
                versuche++;
            }

            benutzer = getBenuzter(benutzerListe, benutzername);
            benutzer.setIstAngemeldet(true);
            benutzer.setLetzterLogin(LocalDateTime.now().toString());

        } else if (auswahl.equals("2")){
            String benutzername;
            char[] erstesPasswort;
            char[] zweitesPasswort;
            console.printf("\nRegistrieren\n");
            console.printf("\tBitte geben Sie Ihre Zugangsdaten ein.\n\n");
            benutzername = console.readLine("\tBenutzername: ");

            while (sucheBenuzter(benutzerListe, benutzername)) {
                console.printf("\tBenutzer existiert schon. Bitte wählen Sie einen anderen Benutzernamen.\n\n");
                benutzername = console.readLine("\tBenutzername: ");
            }

            erstesPasswort = Crypto.encryptViginere(console.readPassword("\tPasswort: "));
            zweitesPasswort = Crypto.encryptViginere(console.readPassword("\tPasswort wiederholen: "));
            while (!Arrays.equals(erstesPasswort, zweitesPasswort)) {
                console.printf("\tDie Passwörter stimmen nicht überein. Bitte geben Sie sie erneut ein.\n\n");
                erstesPasswort = Crypto.encryptViginere(console.readPassword("\tPasswort: "));
                zweitesPasswort = Crypto.encryptViginere(console.readPassword("\tPasswort wiederholen: "));
            }

            Benutzer neuerBenutzer = new Benutzer(benutzername, erstesPasswort);
            benutzerListe.insert(neuerBenutzer);
            console.printf("\nBenutzer %s erstellt.\n\n", benutzername);
            benutzer = neuerBenutzer;
            benutzer.setIstAngemeldet(true);
            benutzer.setLetzterLogin(LocalDateTime.now().toString());
        }

        boolean systemLäuft = true;

        while (systemLäuft) {
            console.printf("\nHallo %s! Sie sind angemeldet.\n\n", benutzer.getBenutzername());
            // Arbeitsumgebung des Benutzers starten.
            // ...
            // Benutzer hat sich abgemeldet.
            console.printf("Auf Wiedersehen.\n");
            systemLäuft = !console.readLine("System herunterfahren? [j/n] ").equals("j");
        }

        // speicherBenutzerlisteBinär(benutzerListe);
        speicherBenutzerlisteKlartext(benutzerListe);

    }

    public static Benutzer getBenuzter(BenutzerListe benutzerListe, String benutzername) {
        return benutzerListe.getBenutzer(benutzername);
    }

    public static boolean sucheBenuzter(BenutzerListe benutzerListe, String benutzername) {
        return !benutzerListe.select(benutzername).equals("");
    }

    public static boolean checkPasswort(Benutzer benutzer, char[] passwort) {
        return Arrays.equals(benutzer.getPasswort(), passwort);
    }

    public static boolean authenticate(BenutzerListe benutzerListe, String name, char[] cryptoPw) {
        Benutzer benutzer = benutzerListe.getBenutzer(name);
        if (benutzer != null) {
            if (checkPasswort(benutzer, cryptoPw)){
                return true;
            }
        }
        return false;
    }

    public static BenutzerListe ladeBenutzerlisteBinär() throws Exception {
        BenutzerListe benutzerListe = new BenutzerListe();
        Path benutzerListePfad = Paths.get("BenutzerListe.bin");

        if (Files.isRegularFile(benutzerListePfad)) {
            ObjectInputStream benutzerListeDatei = new ObjectInputStream(new FileInputStream("BenutzerListe.bin"));
            benutzerListe = (BenutzerListe)benutzerListeDatei.readObject();
            benutzerListeDatei.close();
        } else {
            benutzerListe = new BenutzerListe();
            benutzerListe.insert(new Benutzer("Paula", Crypto.encryptViginere("paula".toCharArray())));
            benutzerListe.insert(new Benutzer("Adam37", Crypto.encryptViginere("adam37".toCharArray())));
            benutzerListe.insert(new Benutzer("Darko", Crypto.encryptViginere("darko".toCharArray())));
            char [] viginereAdmin = Crypto.encryptViginere(Crypto.decryptCaesar(new char[]{36, 61, 72, 72, 75, 13, 14, 15}));
            benutzerListe.insert(new Benutzer("Admin", viginereAdmin));
        }

        console.printf("-binärtest->\n%s<-binärtest-\n\n", benutzerListe.select());
        return benutzerListe;
    }

    public static void speicherBenutzerlisteBinär(BenutzerListe benutzerListe) throws Exception {
        ObjectOutputStream benutzerListeDatei = new ObjectOutputStream(new FileOutputStream("BenutzerListe.bin"));
        benutzerListeDatei.writeObject(benutzerListe);
        benutzerListeDatei.close();
        console.printf("-binärtest->\n%s<-binärtest-\n\n", benutzerListe.select());
    }

    public static BenutzerListe ladeBenutzerListeKlartext() throws Exception {
        BenutzerListe benutzerListe = new BenutzerListe();
        Path benutzerListePfad = Paths.get("BenutzerListe.txt");

        if (Files.isRegularFile(benutzerListePfad)) {
            Stream<String> allLines = Files.lines(benutzerListePfad);
            List<String> lineList = allLines.collect(Collectors.toList());

            allLines.close();

            for (String line : lineList) {
                benutzerListe.insert(new Benutzer(line));
            }
        } else {
            benutzerListe = new BenutzerListe();
            benutzerListe.insert(new Benutzer("Paula", Crypto.encryptViginere("paula".toCharArray())));
            benutzerListe.insert(new Benutzer("Adam37", Crypto.encryptViginere("adam37".toCharArray())));
            benutzerListe.insert(new Benutzer("Darko", Crypto.encryptViginere("darko".toCharArray())));
            char [] viginereAdmin = Crypto.encryptViginere(Crypto.decryptCaesar(new char[]{36, 61, 72, 72, 75, 13, 14, 15}));
            benutzerListe.insert(new Benutzer("Admin", viginereAdmin));
        }

        console.printf("-klartexttest->\n%s<-klartexttest-\n\n", benutzerListe.select());
        return benutzerListe;
    }

    public static void speicherBenutzerlisteKlartext(BenutzerListe benutzerListe) throws Exception {
        String stringObject = benutzerListe.toStringObject();
        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream("BenutzerListe.txt"), "UTF-8");

        try {
            writer.write(stringObject);
        } finally {
            writer.close();
        }
        console.printf("-klartexttest->\n%s<-klartexttest-\n\n", benutzerListe.select());
    }
}