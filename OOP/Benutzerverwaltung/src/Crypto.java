import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Crypto {
    private static int cryptoKey = 65500;
    private static char[] keyWord = "Cry".toCharArray();

    public static char[] encryptCaesar(char[] s) {
        char[] encrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            encrypted[i] = (char)((s[i] + cryptoKey) % 128);
        }
        return encrypted;
    }

    public static char[] decryptCaesar(char[] s) {
        char[] decrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            decrypted[i] = (char)((s[i] + 128 - (cryptoKey % 128)) % 128);
        }
        return decrypted;
    }

    public static char[] encryptViginere(char[] s) {
        char[] encrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            encrypted[i] = (char)((s[i] + keyWord[i % keyWord.length]) % 128);
        }
        return encrypted;
    }

    public static char[] decryptViginere(char[] s) {
        char[] decrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            decrypted[i] = (char)((s[i] + 128 - keyWord[i % keyWord.length]) % 128);
        }
        return decrypted;
    }

    public static String encodeBase64(char[] s) {
        String encodedString = Base64.getEncoder().encodeToString(new String(s).getBytes(StandardCharsets.UTF_8));
        return encodedString;
    }

    public static char[] decodeBase64(String s) {
        String decodedString = new String(Base64.getDecoder().decode(s.getBytes(StandardCharsets.UTF_8)));
        return decodedString.toCharArray();
    }
}