import java.io.Serializable;

public class Benutzer implements Serializable {

    private static final long serialVersionUID = 6982836522061307580L;

    private String vorname;
    private String nachname;
    private String benutzername;
    private char[] passwort;
    private String email;
    private boolean istAdmin;
    private boolean istAngemeldet;
    private String letzterLogin;
    private Benutzer next;

    Benutzer(String benutzername, char[] passwort) {
        this.benutzername = benutzername;
        this.passwort = passwort;
        this.vorname = "";
        this.nachname = "";
        this.email = "";
        this.istAdmin = false;
        this.istAngemeldet = false;
        this.letzterLogin = "";
        this.next = null;
    }

    public Benutzer(String vorname, String nachname, String benutzername, char[] passwort, String email) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.benutzername = benutzername;
        this.passwort = passwort;
        this.email = email;
        this.istAdmin = false;
        this.istAngemeldet = false;
        this.letzterLogin = "";
        this.next = null;
    }

    public Benutzer(String stringObject) {
        String[] attributes = stringObject.split(";", -1);

        if (attributes.length < 8) {
            throw new IllegalArgumentException("Zu wenig Attribute in Benutzer-Stringobjekt.");
        }
        this.vorname = attributes[0].isEmpty() ? "" : attributes[0];
        this.nachname = attributes[1].isEmpty() ? "" : attributes[1];
        this.benutzername = attributes[2].isEmpty() ? "" : attributes[2];
        this.passwort = attributes[3].isEmpty() ? null : Crypto.decodeBase64(attributes[3]);
        this.email = attributes[4].isEmpty() ? "" : attributes[4];
        this.istAdmin = attributes[5].equals("true") ? true : false;
        this.istAngemeldet = attributes[6].equals("true") ? true : false;
        this.letzterLogin = attributes[7].isEmpty() ? "" : attributes[7];
    }

     public boolean hasName(String benutzername) {
        return benutzername.equals(this.benutzername);
    }

    public String toString(){
        String s = "";
        s += benutzername + " ";
        s += new String(passwort);
        return s;
    }

    public String toStringObject() {
        String stringObject = "";

        stringObject += vorname + ";";
        stringObject += nachname + ";";
        stringObject += benutzername + ";";
        stringObject += Crypto.encodeBase64(passwort) + ";";
        stringObject += email + ";";
        stringObject += istAdmin + ";";
        stringObject += istAngemeldet + ";";
        stringObject += letzterLogin;

        return stringObject;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getBenutzername() {
        return benutzername;
    }

    public void setBenutzername(String benutzername) {
        this.benutzername = benutzername;
    }

    public char[] getPasswort() {
        return passwort;
    }

    public void setPasswort(char[] passwort) {
        this.passwort = passwort;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getIstAdmin() {
        return istAdmin;
    }

    public void setIstAdmin(boolean istAdmin) {
        this.istAdmin = istAdmin;
    }

    public boolean getIstAngemeldet() {
        return istAngemeldet;
    }

    public void setIstAngemeldet(boolean istAngemeldet) {
        this.istAngemeldet = istAngemeldet;
    }

    public String getLetzterLogin() {
        return letzterLogin;
    }

    public void setLetzterLogin(String letzterLogin) {
        this.letzterLogin = letzterLogin;
    }

    public Benutzer getNext() {
        return next;
    }

    public void setNext(Benutzer next) {
        this.next = next;
    }
}