public class Pkw {
    private String modell;
    private String hupenKlang;
    private double geschwindigkeit;
    private double richtung;

    public Pkw(String modell, String hupenKlang) {
        this.modell = modell;
        this.hupenKlang = hupenKlang;
        this.geschwindigkeit = 0.0;
        this.richtung = 0.0;
    }

    public void fahre(double geschwindigkeit) {
        if (this.modell == "2CV" && geschwindigkeit >= 180.0) {
            System.out.println("Dieses Modell kann nicht so schnell fahren.");
        }else {
            this.geschwindigkeit = geschwindigkeit;
            System.out.println(this.modell + " fährt mit einer Geschwindigkeit von " + this.geschwindigkeit + " km/h.");
        }
    }

    public void lenke(double richtung) {
        this.richtung = richtung;
        System.out.println(this.modell + " fährt in Richtung " + this.geschwindigkeit + " Grad");
    }

    public void hupe(double dauer) {
        while(dauer > 0.0) {
            System.out.print(this.hupenKlang + " ");
            dauer -= 1.0;
        }
        System.out.println();
    }
}
