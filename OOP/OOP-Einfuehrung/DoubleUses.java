public class DoubleUses {
    public static void main(String[] args) {
        // Check if num value is within double range
        Double num = 7777.77;
        if (num >= Double.MIN_VALUE && num <= Double.MAX_VALUE) {
            System.out.println(num + " is within the value range of Double.");
        }

        // Check if two Double object are equal
        Double otherNum = 666.666;
        System.out.printf("%.3f == %.3f is %s\n", num, otherNum, num.compareTo(otherNum) == 0 ? "True" : "False");
    }
}
