public class HundTest {
    public static void main(String[] args) {
        Hund meinHund = new Hund("Strakö");
        Hund testHund = new Hund("Fido", "Strakö", 70.5);

        meinHund.setName("Fridolin");

        meinHund.belle();
        testHund.belle();
    }
}
