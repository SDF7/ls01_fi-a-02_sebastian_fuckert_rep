public class PkwTest {
    public static void main(String[] args) {
        Pkw audi = new Pkw("A3", "MEPMEP");
        Pkw bmw = new Pkw("Z4", "MÖPMÖP");
        Pkw ente53 = new Pkw("2CV", "QUACKQUACK");

        audi.fahre(55.0);
        bmw.fahre(180.0);
        ente53.fahre(180.0);

        audi.lenke(45.0);
        bmw.lenke(90.0);
        ente53.lenke(180.0);
        audi.hupe(1.0);
        bmw.hupe(1.5);
        ente53.hupe(2.7);
    }
}
