import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		String artikel = liesString("Was möchten Sie bestellen?");

		int anzahl = liesInt("Geben Sie die Anzahl ein:");

		double preis = liesDouble("Geben Sie den Bruttopreis ein:");

		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");

		// Verarbeiten
		double bruttogesamtpreis = berechneGesamtbruttopreis(anzahl, preis);
		double nettogesamtpreis = berechneGesamtnettopreis(bruttogesamtpreis, mwst);

		// Ausgeben
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}

	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		return myScanner.next();
	}

	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		return myScanner.nextInt();
	}

	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		return myScanner.nextDouble();
	}

	public static double berechneGesamtbruttopreis(int anzahl, double nettopreis) {
		return anzahl * nettopreis;
	}

	public static double berechneGesamtnettopreis(double bruttogesamtpreis, double mwst) {
		return bruttogesamtpreis * (1 + mwst / 100);
	}

	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f %n", artikel, anzahl, bruttogesamtpreis);
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, nettogesamtpreis, mwst, "%");
	}

}
